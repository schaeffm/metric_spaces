theory Metric_Arith_Examples
imports Metric_Arith
begin

declare [[metric_trace]]

(* simple examples *)

lemma "\<exists>x::'a::metric_space. x=x"
  by metric
lemma "\<forall>(x::'a::metric_space). \<exists>y. x = y"
  by metric

(* reasoning with "dist x y = 0 \<longleftrightarrow> x = y" *)
lemma "\<exists>x y. dist x y = 0"
  by metric

lemma "\<exists>y. dist x y = 0"
  by metric

lemma "dist x y = 0 \<longleftrightarrow> x = y"
  by metric

lemma "0 = dist x y \<Longrightarrow> x = y"
  by metric

lemma "x\<noteq>y \<Longrightarrow> dist x y \<noteq> 0"
  by metric

lemma "x\<noteq>y \<Longrightarrow> dist x y > 0"
  by metric

lemma "\<exists>y. dist x y \<noteq> 1"
  by metric

lemma "x=y \<longleftrightarrow> dist x x = dist y x \<and> dist x y = dist y y"
  by metric

lemma "dist a b \<noteq> dist a c \<Longrightarrow> b \<noteq> c"
  by metric

(* reasoning with positive semidefiniteness *)
lemma "dist x y \<ge> 0"
  by metric

lemma "dist y x + c \<ge> c"
  by metric

lemma "dist x y + dist x z \<ge> 0"
  by metric

lemma
  fixes x::"('a::metric_space)"
  shows "dist x y \<ge> v \<Longrightarrow> dist x y + dist (a::'a) b \<ge> v"
  by metric

lemma "dist x y < 0 \<longrightarrow> P"
  by metric

(* reasoning with the triangle inequality *)
lemma "dist a c \<le> dist a b + dist b c"
  by metric

lemma "dist a c \<le> dist a b + dist c b"
  by metric

lemma "dist a 0 + dist 0 b \<ge> dist a b"
  by metric

lemma "dist a d \<le> dist a b + dist b c + dist c d"
  by metric

lemma "dist a e \<le> dist a b + dist b c + dist c d + dist d e"
  by metric

lemma "dist x y < e \<Longrightarrow> dist z y < f \<Longrightarrow> dist x z < e + f"
  by metric

lemma "max (dist x y) \<bar>dist x z - dist z y\<bar> = dist x y"
  by metric

lemma "\<bar>dist x z - dist z y\<bar> \<le> dist x y"
  by metric

lemma
  "dist w x < e/3 \<Longrightarrow> dist x y < e/3 \<Longrightarrow> dist y z < e/3 \<Longrightarrow> dist w x < e"
  by metric

lemma "dist x y < e/2 \<Longrightarrow> dist z y < e/2 \<Longrightarrow> dist x z < e"
  by metric

lemma "dist w x < e/4 \<Longrightarrow> dist x y < e/4 \<Longrightarrow> dist y z < e/2 \<Longrightarrow> dist w z < e"
  by metric

(* more complex examples *)
lemma "dist x y \<le> e \<Longrightarrow> dist x z \<le> e \<Longrightarrow> dist y z \<le> e
  \<Longrightarrow> p \<in> (cball x e \<union> cball y e \<union> cball z e) \<Longrightarrow> dist p x \<le> 2*e"
  by metric

lemma from_hol_light:
"\<not> disjnt (ball x r) (ball y s)
 \<Longrightarrow> (\<forall>p q. p \<in> (ball x r \<union> ball y s) \<and>
           q \<in> (ball x r \<union> ball y s)
 \<longrightarrow> dist p q < 2*(r+s))"
  unfolding disjnt_iff
  by metric

lemma "dist x y \<ge> r + s \<Longrightarrow> ball x r \<inter> ball y s = {}"
  by metric
  
lemma "dist x y > r + s \<Longrightarrow> cball x r \<inter> cball y s = {}"
  by metric

lemma "dist x y \<le> e \<Longrightarrow> z \<in> ball x f \<Longrightarrow> dist z y < e + f"
  by metric

lemma "dist x y = r/2 \<Longrightarrow> (\<forall>z. dist x z < r / 4 \<longrightarrow> dist y z \<le> 3 * r / 4)"
  by metric

lemma "s \<ge> 0 \<Longrightarrow> t \<ge> 0 \<Longrightarrow> z \<in> (ball x s) \<union> (ball y t) \<Longrightarrow> dist z y \<le> dist x y + s + t"
  by metric

lemma "0 < r \<Longrightarrow> ball x r \<subseteq> ball y s \<Longrightarrow> ball x r \<subseteq> ball z t \<Longrightarrow> dist y z \<le> s+t"
  by metric

(* non-trivial quantifier structure *)
lemma "\<exists>x. \<forall>r\<le>0. \<exists>z. dist x z \<ge> r"
  by metric

lemma "\<And>a r x y. dist x a + dist a y = r \<Longrightarrow> \<forall>z. r \<le> dist x z + dist z y \<Longrightarrow> dist x y = r"
  by metric
end