theory Metric_Applications

imports "HOL-Analysis.Analysis" Metric_Arith
begin
declare [[metric_trace]]

(* Analysis/Complex_Transcendental, l. 267 *)
lemma
  assumes "dist z (y + 2 * of_int n * of_real pi * \<i>) < pi" "dist z y < pi" 
  shows "dist y (y + 2 * of_int n * of_real pi * \<i>) < pi+pi"
  using assms
  by metric

(* original proof*)
lemma
  assumes "dist z (y + 2 * of_int n * of_real pi * \<i>) < pi" "dist z y < pi" 
  shows "dist y (y + 2 * of_int n * of_real pi * \<i>) < pi+pi"
  using assms
  using dist_commute_lessI dist_triangle_less_add by blast

(* Analysis/Conformal_Mappings, l. 400 *)
declare pre_arith_simps [pre_arith del]

lemma
  fixes \<gamma> w z \<epsilon> \<xi> f
  defines "\<epsilon> \<equiv> norm (f w - f \<xi>) / 3"
  assumes \<gamma>: "\<gamma> \<in> ball (f \<xi>) \<epsilon>" 
  assumes w: "(\<And>z. norm (\<xi> - z) = r \<Longrightarrow> norm (f w - f \<xi>) \<le> norm(f z - f \<xi>))"
  assumes that: "cmod (\<xi> - z) = r"
  shows "cmod (\<gamma> - f \<xi>) < cmod (\<gamma> - f z)"
  using w[OF that] \<gamma> \<epsilon>_def
  unfolding dist_norm [symmetric]
  by metric

declare pre_arith_simps [pre_arith]

(* original proof *)
lemma
  fixes \<gamma> w z \<epsilon> \<xi> f
  defines "\<epsilon> \<equiv> norm (f w - f \<xi>) / 3"
  assumes \<gamma>: "\<gamma> \<in> ball (f \<xi>) \<epsilon>" 
  assumes w: "(\<And>z. norm (\<xi> - z) = r \<Longrightarrow> norm (f w - f \<xi>) \<le> norm (f z - f \<xi>))"
  assumes that: "cmod (\<xi> - z) = r"
  shows "cmod (\<gamma> - f \<xi>) < cmod (\<gamma> - f z)"
proof -
  have lt: "cmod (f w - f \<xi>) / 3 < cmod (\<gamma> - f z)"
    using w [OF that] \<gamma>
    using dist_triangle2 [of "f \<xi>" "\<gamma>"  "f z"] 
    using dist_triangle2 [of "f \<xi>" "f z" \<gamma>]
    by (simp add: \<epsilon>_def dist_norm norm_minus_commute)
  show ?thesis
    by (metis \<epsilon>_def dist_commute dist_norm less_trans lt mem_ball \<gamma>)
qed

(* Analysis/Connected, l. 1446 *)
(* instance metric_space \<subseteq> t4_space *)
lemma
  assumes "x\<in>S" "y\<in>T"
  shows  "(ball x ((infdist x T)/2)) \<inter> (ball y ((infdist y S)/2)) = {}"
  using infdist_le[OF \<open>x \<in> S\<close>, of y] infdist_le[OF \<open>y \<in> T\<close>, of x]
  by metric

(* original proof *)
lemma
  assumes "x\<in>S" "y\<in>T"
  shows  "(ball x ((infdist x T)/2)) \<inter> (ball y ((infdist y S)/2)) = {}"
proof (auto)
  fix z assume H: "dist x z * 2 < infdist x T" "dist y z * 2 < infdist y S"
  have "2 * dist x y \<le> 2 * dist x z + 2 * dist y z"
    using dist_triangle[of x y z] by (auto simp add: dist_commute)
  also have "... < infdist x T + infdist y S"
    using H by auto
  finally have "dist x y < infdist x T \<or> dist x y < infdist y S"
    by auto
  then show False
    using infdist_le[OF \<open>x \<in> S\<close>, of y] infdist_le[OF \<open>y \<in> T\<close>, of x] by (auto simp add: dist_commute)
qed

(* Analysis/Connected, l. 1498 *)
(* compact_infdist_le *)
lemma
  assumes b: "\<And>x. x \<in> A \<Longrightarrow> dist x0 x \<le> b" "closed A"
  assumes le: "infdist y A \<le> e"
  assumes z: "z \<in> A" "infdist y A = dist y z"
  shows "dist x0 y \<le> b + e"
  using assms
  by metric

(* original proof *)
lemma
  assumes b: "\<And>x. x \<in> A \<Longrightarrow> dist x0 x \<le> b" "closed A"
  assumes le: "infdist y A \<le> e"
  assumes z: "z \<in> A" "infdist y A = dist y z"
  shows "dist x0 y \<le> b + e"
  using le z b
proof -
  have "dist x0 y \<le> dist y z + dist x0 z"
    by (metis dist_commute dist_triangle)
  also have "dist y z \<le> e" using le z by simp
  also have "dist x0 z \<le> b" using b z by simp
  finally show ?thesis by arith
qed

(* Analysis/Connected, l. 1870 *)
lemma uniformly_continuous_on_closure:
  fixes f :: "'a::metric_space \<Rightarrow> 'b::metric_space"
  assumes ucont: "uniformly_continuous_on S f"
    and cont: "continuous_on (closure S) f"
  shows "uniformly_continuous_on (closure S) f"
  unfolding uniformly_continuous_on_def
proof (intro allI impI)
  fix e::real
  assume "0 < e"
  then obtain d::real
    where "d>0"
      and d: "\<And>x x'. \<lbrakk>x\<in>S; x'\<in>S; dist x' x < d\<rbrakk> \<Longrightarrow> dist (f x') (f x) < e/3"
    using ucont [unfolded uniformly_continuous_on_def, rule_format, of "e/3"] by auto
  show "\<exists>d>0. \<forall>x\<in>closure S. \<forall>x'\<in>closure S. dist x' x < d \<longrightarrow> dist (f x') (f x) < e"
  proof (rule exI [where x="d/3"], clarsimp simp: \<open>d > 0\<close>)
    fix x y
    assume x: "x \<in> closure S" and y: "y \<in> closure S" and dyx: "dist y x * 3 < d"
    obtain d1::real where "d1 > 0"
      and d1: "\<And>w. \<lbrakk>w \<in> closure S; dist w x < d1\<rbrakk> \<Longrightarrow> dist (f w) (f x) < e/3"
      using cont [unfolded continuous_on_iff, rule_format, of "x" "e/3"] \<open>0 < e\<close> x by auto
    obtain x' where "x' \<in> S" and x': "dist x' x < min d1 (d / 3)"
      using closure_approachable [of x S]
      by (metis \<open>0 < d1\<close> \<open>0 < d\<close> divide_pos_pos min_less_iff_conj x zero_less_numeral)
    obtain d2::real where "d2 > 0"
      and d2: "\<forall>w \<in> closure S. dist w y < d2 \<longrightarrow> dist (f w) (f y) < e/3"
      using cont [unfolded continuous_on_iff, rule_format, of "y" "e/3"] \<open>0 < e\<close> y by auto
    obtain y' where "y' \<in> S" and y': "dist y' y < min d2 (d / 3)"
      using closure_approachable [of y S]
      by (metis \<open>0 < d2\<close> \<open>0 < d\<close> divide_pos_pos min_less_iff_conj y zero_less_numeral)
    have "dist (f x') (f y') < e/3"
      using y' dyx d[OF \<open>y' \<in> S\<close> \<open>x' \<in> S\<close>] x' by metric
    moreover have x_x'_third: "dist (f x') (f x) < e/3" 
      using \<open>x' \<in> S\<close> closure_subset x' d1
      by (simp add: closure_def)
    moreover have y'_y_third: "dist (f y') (f y) < e/3" using \<open>y' \<in> S\<close> closure_subset y' d2
      by (simp add: closure_def)
    ultimately show "dist (f y) (f x) < e"
      by metric
  qed
qed

(* Analysis/Connected, l. 1993, 0.13s after *)
lemma uniformly_continuous_on_extension_on_closure:
  fixes f::"'a::metric_space \<Rightarrow> 'b::complete_space"
  assumes uc: "uniformly_continuous_on X f"
  obtains g where "uniformly_continuous_on (closure X) g" "\<And>x. x \<in> X \<Longrightarrow> f x = g x"
    "\<And>Y h x. X \<subseteq> Y \<Longrightarrow> Y \<subseteq> closure X \<Longrightarrow> continuous_on Y h \<Longrightarrow> (\<And>x. x \<in> X \<Longrightarrow> f x = h x) \<Longrightarrow> x \<in> Y \<Longrightarrow> h x = g x"
proof -
  from uc have cont_f: "continuous_on X f"
    by (simp add: uniformly_continuous_imp_continuous)
  obtain y where y: "(f \<longlongrightarrow> y x) (at x within X)" if "x \<in> closure X" for x
    apply atomize_elim
    apply (rule choice)
    using uniformly_continuous_on_extension_at_closure[OF assms]
    by metis
  let ?g = "\<lambda>x. if x \<in> X then f x else y x"

  have "uniformly_continuous_on (closure X) ?g"
    unfolding uniformly_continuous_on_def
  proof safe
    fix e::real assume "e > 0"
    define e' where "e' \<equiv> e / 3"
    have "e' > 0" using \<open>e > 0\<close> by (simp add: e'_def)
    from uc[unfolded uniformly_continuous_on_def, rule_format, OF \<open>0 < e'\<close>]
    obtain d where "d > 0" and d: "\<And>x x'. x \<in> X \<Longrightarrow> x' \<in> X \<Longrightarrow> dist x' x < d \<Longrightarrow> dist (f x') (f x) < e'"
      by auto
    define d' where "d' = d / 3"
    have "d' > 0" using \<open>d > 0\<close> by (simp add: d'_def)
    show "\<exists>d>0. \<forall>x\<in>closure X. \<forall>x'\<in>closure X. dist x' x < d \<longrightarrow> dist (?g x') (?g x) < e"
    proof (safe intro!: exI[where x=d'] \<open>d' > 0\<close>)
      fix x x' assume x: "x \<in> closure X" and x': "x' \<in> closure X" and dist: "dist x' x < d'"
      then obtain xs xs' where xs: "xs \<longlonglongrightarrow> x" "\<And>n. xs n \<in> X"
        and xs': "xs' \<longlonglongrightarrow> x'" "\<And>n. xs' n \<in> X"
        by (auto simp: closure_sequential)
      have "\<forall>\<^sub>F n in sequentially. dist (xs' n) x' < d'"
        and "\<forall>\<^sub>F n in sequentially. dist (xs n) x < d'"
        by (auto intro!: \<open>0 < d'\<close> order_tendstoD tendsto_eq_intros xs xs')
      moreover
      have "(\<lambda>x. f (xs x)) \<longlonglongrightarrow> y x" if "x \<in> closure X" "x \<notin> X" "xs \<longlonglongrightarrow> x" "\<And>n. xs n \<in> X" for xs x
        using that not_eventuallyD
        by (force intro!: filterlim_compose[OF y[OF \<open>x \<in> closure X\<close>]] simp: filterlim_at)
      then have "(\<lambda>x. f (xs' x)) \<longlonglongrightarrow> ?g x'" "(\<lambda>x. f (xs x)) \<longlonglongrightarrow> ?g x"
        using x x'
        by (auto intro!: continuous_on_tendsto_compose[OF cont_f] simp: xs' xs)
      then have "\<forall>\<^sub>F n in sequentially. dist (f (xs' n)) (?g x') < e'"
        "\<forall>\<^sub>F n in sequentially. dist (f (xs n)) (?g x) < e'"
        by (auto intro!: \<open>0 < e'\<close> order_tendstoD tendsto_eq_intros)
      ultimately
      have "\<forall>\<^sub>F n in sequentially. dist (?g x') (?g x) < e"
      proof eventually_elim
        case (elim n)
        have "dist (f (xs' n)) (f (xs n)) < e'"
          using d[OF \<open>xs n \<in> X\<close> \<open>xs' n \<in> X\<close>] 
            \<open>dist (xs' n) x' < d'\<close> \<open>dist x' x < d'\<close> \<open>dist (xs n) x < d'\<close> d'_def
          by metric
        thus ?case
          using e'_def d'_def 
            \<open>dist (f (xs' n)) (?g x') < e'\<close> \<open>dist (f (xs n)) (?g x) < e'\<close>
          by metric
        oops

(* original proof:
proof eventually_elim
        case (elim n)
        have "dist (?g x') (?g x) \<le>
          dist (f (xs' n)) (?g x') + dist (f (xs' n)) (f (xs n)) + dist (f (xs n)) (?g x)"
          by (metis add.commute add_le_cancel_left dist_commute dist_triangle dist_triangle_le)
        also
        {
          have "dist (xs' n) (xs n) \<le> dist (xs' n) x' + dist x' x + dist (xs n) x"
            by (metis add.commute add_le_cancel_left  dist_triangle dist_triangle_le)
          also note \<open>dist (xs' n) x' < d'\<close>
          also note \<open>dist x' x < d'\<close>
          also note \<open>dist (xs n) x < d'\<close>
          finally have "dist (xs' n) (xs n) < d" by (simp add: d'_def)
        }
*)

(* Analysis/Connected, l. 2693 *)
(* diameter_bounded_bound *)
lemma
  fixes z d
  fixes s :: "'a :: metric_space set"
  assumes s: "Topology_Euclidean_Space.bounded s" "x \<in> s" "y \<in> s"
  assumes z: "\<And>x. x \<in> s \<Longrightarrow> dist z x \<le> d"
  shows "bdd_above (case_prod dist ` (s\<times>s))"
proof (intro bdd_aboveI, safe)
  fix a b
  assume "a \<in> s" "b \<in> s"
  show "dist a b \<le> 2 * d"
    using z[OF \<open>a\<in>s\<close>] z[OF \<open>b\<in>s\<close>]
    by metric
qed

(* original proof *)
lemma
  fixes z d
  fixes s :: "'a :: metric_space set"
  assumes s: "Topology_Euclidean_Space.bounded s" "x \<in> s" "y \<in> s"
  assumes z: "\<And>x. x \<in> s \<Longrightarrow> dist z x \<le> d"
  shows "bdd_above (case_prod dist ` (s\<times>s))"
proof (intro bdd_aboveI, safe)
  fix a b
  assume "a \<in> s" "b \<in> s"
  with z[of a] z[of b] dist_triangle[of a b z]
  show "dist a b \<le> 2 * d"
    by (simp add: dist_commute)
qed

(* Analysis/Connected, l. 2797 *)
lemma diameter_closure:
  assumes "Topology_Euclidean_Space.bounded S"
  shows "diameter(closure S) = diameter S"
proof (rule order_antisym)
  have "False" if "diameter S < diameter (closure S)"
  proof -
    define d where "d = diameter(closure S) - diameter(S)"
    have "d > 0"
      using that by (simp add: d_def)
    then have "diameter(closure(S)) - d / 2 < diameter(closure(S))"
      by simp
    have dd: "diameter (closure S) - d / 2 = (diameter(closure(S)) + diameter(S)) / 2"
      by (simp add: d_def divide_simps)
    have bocl: "Topology_Euclidean_Space.bounded (closure S)"
      using assms by blast
    moreover have "0 \<le> diameter S"
      using assms diameter_ge_0 by blast
    ultimately obtain x y where "x \<in> closure S" "y \<in> closure S" and xy: "diameter(closure(S)) - d / 2 < dist x y"
      using diameter_bounded(2) [OF bocl, rule_format, of "diameter(closure(S)) - d / 2"] \<open>d > 0\<close> d_def by auto
    then obtain x' y' where x'y': "x' \<in> S" "dist x' x < d/4" "y' \<in> S" "dist y' y < d/4"
      using closure_approachable
      by (metis \<open>0 < d\<close> zero_less_divide_iff zero_less_numeral)
    then have "dist x' y' \<le> diameter S"
      using assms diameter_bounded_bound by blast
    then show ?thesis
      using xy d_def x'y'
      by metric
  qed
  then show "diameter (closure S) \<le> diameter S"
    by fastforce
next
  show "diameter S \<le> diameter (closure S)"
    by (simp add: assms bounded_closure closure_subset diameter_subset)
qed

(* original proof:
then have "dist x' y' \<le> diameter S"
      using assms diameter_bounded_bound by blast
    with x'y' have "dist x y \<le> d / 4 + diameter S + d / 4"
      by (meson add_mono_thms_linordered_semiring(1) dist_triangle dist_triangle3 less_eq_real_def order_trans)
    then show ?thesis
      using xy d_def by linarith
*)


lemma cball_subset_ball_iff: "cball a r \<subseteq> ball a' r' \<longleftrightarrow> dist a a' + r < r' \<or> r < 0"
  (is "?lhs \<longleftrightarrow> ?rhs")
  for a :: "'a::euclidean_space"
proof
  assume ?lhs
  then show ?rhs
  proof (cases "r < 0")
    case True then
    show ?rhs by simp
  next
    case False
    then have [simp]: "r \<ge> 0" by simp
    have "norm (a - a') + r < r'"
    proof (cases "a = a'")
      case True
      then show ?thesis
        using subsetD [where c = "a + r *\<^sub>R (SOME i. i \<in> Basis)", OF \<open>?lhs\<close>] subsetD [where c = a, OF \<open>?lhs\<close>]
        by (force simp: SOME_Basis dist_norm)
    next
      case False
      have False if "norm (a - a') + r \<ge> r'"
      proof -
        from that have "\<bar>r' - norm (a - a')\<bar> \<le> r"
          using \<open>?lhs\<close> \<open>0 \<le> r\<close>
          unfolding dist_norm[symmetric]
          by metric
        then show ?thesis
          using subsetD [where c = "a + (r' / norm(a - a') - 1) *\<^sub>R (a - a')", OF \<open>?lhs\<close>] \<open>a \<noteq> a'\<close>
          by (simp add: dist_norm field_simps)
            (simp add: diff_divide_distrib scaleR_left_diff_distrib)
      qed
      then show ?thesis by force
    qed
    then show ?rhs by (simp add: dist_norm)
  qed
  oops

(* original proof:
 by (simp split: abs_split)
            (metis \<open>0 \<le> r\<close> \<open>?lhs\<close> centre_in_cball dist_commute dist_norm less_asym mem_ball subset_eq)
*)

(* Analysis/Equivalence_Lebesgue_Henstock_Integration, l. 14 *)
lemma ball_trans:
  assumes "y \<in> ball z q" "r + q \<le> s" shows "ball y r \<subseteq> ball z s"
  using assms by metric

(* original proof *)
lemma
  assumes "y \<in> ball z q" "r + q \<le> s" shows "ball y r \<subseteq> ball z s"
proof safe
  fix x assume x: "x \<in> ball y r"
  have "dist z x \<le> dist z y + dist y x"
    by (rule dist_triangle)
  also have "\<dots> < s"
    using assms x
    by auto
  finally show "x \<in> ball z s"
    by simp
qed


(* Limits, l. 515 *)
lemma tendsto_dist [tendsto_intros]:
  fixes l m :: "'a::metric_space"
  assumes f: "(f \<longlongrightarrow> l) F"
    and g: "(g \<longlongrightarrow> m) F"
  shows "((\<lambda>x. dist (f x) (g x)) \<longlongrightarrow> dist l m) F"
proof (rule tendstoI)
  fix e :: real
  assume "0 < e"
  then have e2: "0 < e/2" by simp
  from tendstoD [OF f e2] tendstoD [OF g e2]
  show "eventually (\<lambda>x. dist (dist (f x) (g x)) (dist l m) < e) F"
  proof (eventually_elim)
    case (elim x)
    then show "dist (dist (f x) (g x)) (dist l m) < e"
      unfolding dist_real_def
      by metric
  qed
qed

(* original proof *)
lemma
  fixes l m :: "'a::metric_space"
  assumes f: "(f \<longlongrightarrow> l) F"
    and g: "(g \<longlongrightarrow> m) F"
  shows "((\<lambda>x. dist (f x) (g x)) \<longlongrightarrow> dist l m) F"
proof (rule tendstoI)
  fix e :: real
  assume "0 < e"
  then have e2: "0 < e/2" by simp
  from tendstoD [OF f e2] tendstoD [OF g e2]
  show "eventually (\<lambda>x. dist (dist (f x) (g x)) (dist l m) < e) F"
  proof (eventually_elim)
    case (elim x)
    then show "dist (dist (f x) (g x)) (dist l m) < e"
      unfolding dist_real_def
      using dist_triangle2 [of "f x" "g x" "l"]
        and dist_triangle2 [of "g x" "l" "m"]
        and dist_triangle3 [of "l" "m" "f x"]
        and dist_triangle [of "f x" "m" "g x"]
      by arith
  qed
qed

end